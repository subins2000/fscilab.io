# Simple guide to contributing to this repo.
- Fork the repo from FSCI Gitlab. 
- Clone your forked repo to your local machine. 
    
    ```sh
    git remote add origin git@gitlab.com:<your-username>/fsci.gitlab.io.git
    ```
  The above example uses `ssh` to clone the repository.
  You can use `ssh` or `https` to clone the repository but using `ssh` is prefered as it is more secure. Here's the [documentation](https://docs.gitlab.com/ee/ssh/) from Gitlab on how to create and use `ssh` with Git and Gitlab.

- Next, let's create a branch to work on.
    ```sh
    git checkout -b branch-name
    ```
- Now you can start working on making the changes you intended to make.
- Once done, you can add/stage your changes using the command below.
    ```sh
    git add filepath1 filepath2
    ```
- Now you must commit your changes. 
    ```sh
    git commit
    ```
    This command will open a text editor in your terminal prompting you to write a commit message. Write the commit message, save the file and exit out of the editor. You have now made a commit. 

    A commit message has two parts: a title and a description. The title is a one line summary of what the commit is about and the description provides a litle more information and context about the commit. Some commits can be very straight forward and might not require a detailed description, in such cases the description can be omitted.

    It is a good idea to sign your commits with your GPG key. Here's the [documentation](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#signing-commits-with-gpg) from Gitlab on how to create and use GPG keys with Git and Gitlab to sign your commits. This helps us be sure that the person who made the commit is infact you and not someone who broke into your Gitlab account. Signing commits is not mandatory for contributing to this project right now.

- Now that you've made a commit, it's time to push your changes.
    ```sh
    git push -u
    ```

- Once you've pushed your changes to your fork, start a merge request (MR) against the master branch of [fsci.gitlab.io repo under the FSCI namespace](https://gitlab.com/fsci/fsci.gitlab.io). Here's the [documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) from Gitlab on how to create merge requests.

    It's a good idea to tag and request someone to review your merge request. If the merge request is regarding a particular post check for the author of the post and tag the person. If it's reagrding adding features or maintaining the website feel free to tag @avron or  @kskarthik .

    Once reviewed the changes will be merged to the `master` branch.
