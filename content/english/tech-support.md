---
title: "Tech Support"
date: 2022-06-15T12:56:48+05:30
author: false
draft: false 
---
We also provide technical support on Free Software. We have separate chat groups for technical support. Feel free to drop by and ask technical questions in our [matrix group](https://matrix.to/#/#fsci-support:poddery.com) or in our [XMPP group](https://join.jabber.network/#fsci-support@groups.poddery.com?join). If you are not using Matrix or XMPP already, the easiest way is to install [Quicksy app](https://quicksy.im) on Android and join the XMPP group.

Keep in mind that the technical support is provided by the volunteers in their free time, so you might not get a reply or might get a delayed response. We are a Free Software community and we may not be able to provide technical support on proprietary software from Microsoft, Adobe, Meta, etc. 
