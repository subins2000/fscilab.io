---
title: "Prav App Project: Challenging the Monopoly of WhatsApp"
date: 2022-04-09T18:16:49+05:30
author: "false"
tags: ["prav", "xmpp", "privacy", "free-software"]
draft: false
discussionlink: "https://codema.in/d/xOc0kCw7/promote-prav-app-project-officially-as-fsci"
---
PravApp project is a plan to get a lot of people to invest small amounts to run an interoperable messaging service that is easier to join and discover contacts.

<img src="/img/prav-logo.png"  style="height:200px;">

It will be based on [Quicksy](https://quicksy.im) which gives a similar experience to WhatsApp without locking users to its service by interoperability with any [XMPP](https://xmpp.org) service provider.

The prav project wants to offer this service under a Multi State Cooperative Society in India, and they need 50 members each from at least two Indian states.

In addition to the software freedom and interoperability, the Prav project will give users control over its privacy policy. The cooperative society ensures democratic decision making as the members of the cooperative society will have equal voting power.

<img src="/img/mobile.png" style="width:400px;">

[Prav's Mastodon handle](https://venera.social/display/85a863ed-1662-496c-4d90-a1a537464326) summarized the project very well:

> Imagine there are two dining mess (where students eat) in your college, one in which the menu is decided by the administration, and the other one in which the menu is decided by the students. The administration mess is under their policies over which students have no control, and the menu along with any other policies can be changed anytime without asking students, whereas every decision in the student mess is taken collectively by students.
>
> Which one would you prefer as a student?
>
> Prav project is like the student mess in the above example, where users control the Privacy Policy of the service.

Learn more at [prav.app](https://prav.app).

Check out this video for an introduction in the Malayalam language:

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/e3eae17b-bd71-4a5d-af68-9aca90118765" frameborder="0" allowfullscreen></iframe>
