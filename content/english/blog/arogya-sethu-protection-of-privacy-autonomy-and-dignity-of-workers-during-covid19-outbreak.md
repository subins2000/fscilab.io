---
title: "Arogya Sethu & Protection of privacy, autonomy and dignity of workers during COVID-19 outbreak"
date: 2020-05-13T13:33:21+05:30
author: "false"
tags: ["arogyasethu"]
discussionlink: https://codema.in/d/vhoA0lCM/aarogya-setu-app-is-released-as-free-software-we-should-respond-to-this-with-a-statement
draft: false
---
 FSCI endorses the letter of representation against Mandatory Use of Aarogya Setu to protect privacy, autonomy and dignity of workers during COVID-19 outbreak, originally drafted by Internet Freedom Foundation. Read the full letter in PDF. [Download](../../docs/Representation_GigWorkers_Aarogya_Setu_(English).pdf)
 
See the original thread on [codema.in](https://codema.in/d/UCOP3ieV/urgent-endorsement-request) for discussions related to this statement.